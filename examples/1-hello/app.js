const result = document.querySelector('.result')

// This will point to functions directory 1-hello,js file
const fetchData = async() => {
    try {
        const { data } = await axios.get('/api/1-hello');
        result.textContent = data;
        console.log(data);
    }catch(error){
        console.log(error.response);
    }
}

fetchData()