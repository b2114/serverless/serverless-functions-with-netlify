require('dotenv').config()

const Airtable = require('airtable-node');
const tableName = "products";

const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY })
  .base(process.env.AIRTABLE_BASE_ID) // ID that refers the base in a workspace
  .table(tableName)  // Table to fetch data inside this base

exports.handler = async (event,context, cb) => {
    try{
        const { records } = await airtable.list()
        const products = records.map((product) => {
            const {id} = product
            const {name, image, price} = product.fields
            const url = image[0].url
            return {id, name, url, price}
        })

        return {
            statusCode: 200,
            body: JSON.stringify(products),
        }
    } catch (error) {
        return {
            statusCode: 500,
            body: "Internal Server Error",
        }
    }
}
