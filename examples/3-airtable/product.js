
const result = document.querySelector('.result')

// This will point to functions directory 2-basic-api.js file
const fetchProduct = async() => {
    result.innerHTML=`<h2>Loading ...</h2>`
    try{
        const id = window.location.search;

        const {data:{fields}} = await axios.get(`/.netlify/functions/3-product${id}`) //! DON'T KNOW WHY DOESN'T RECOGNIZE /api/3-product?id=-----
        const {image, name, price, description} = fields

        result.innerHTML = `  <h1 class="title">${name}</h1>
        <article class="product">
          <img class="product-img"
          src="${image[0].url}"
          alt="${name}"
          />
          <div class="product-info">
            <h5 class="title">${name}</h5>
            <h5 class="price">$${price}</h5>
            <p class="desc">${description}</p>
          </div>
        </article>`
    } catch(error){
        result.innerHTML = `<h4>${error.response.data}</h4>`;
        console.log(error.response);
    }
}

fetchProduct()