# Serverless functions with Netlify

**Run project locally:**

```bash
npm run netlify
```

## Netlify CLI

- Will set up a production environment on our local machine
- Will detect what frontend arfe we using (REACT/GATSBY/...) and will take care of what we need based on the framework


## Serverless function example:

```javascript
exports.handler = async (event,context, cb) => {
    return {
        statusCode: 200,
        body: "Our First Netlify Function Example",
    }
}
```

- `event` will have information about the incoming request
- `context` will explain the context wjhere the function is running
  -  will be helpful when dealing with authentication (netlify euth setup - identity)
  -  can get the info about the user that is accessing the function (guard methods - token)


## Netlify - Deploy

- Team Overview

Add a new site by importing existing project and link that site to a github/gitlab repo

![netlify add new site](./docs/data/netlify_team_view.png)

- Site overview

![overview](./docs/data/netlify_site_view.png)
![overview](./docs/data/netlify_site_view_1.png)

- Deploys

![overview](./docs/data/netlify_deploy.png)

![logs](./docs/data/netlify_deploy_logs.png)

- Functions

![overview](./docs/data/netlify_functions.png)

![functions level - analyses](./docs/data/netlify_functions_usage.png)

NOTES:

- Once we link our source code to the netlify, every time we push new code to the repo, netlify will build and deploy automatically
- Allowing cors will allow other applications to access the resources of our API.
  - Apps can as example get our products data by making a request like: `axios.get('https://serverless-functions-with-netlify.netlify.app/api/2-basic-api')`;
    - For that you just need to return this in the serveless function that you want to allow:
    ```javascript
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
    ```

## Airtable - Database

- Bases

Create your workspace that can handle multiple databases. Should create different workspaces for each project
Add a base and start with your tables

![workspace and bases](./docs/data/airtable_database_workspace_bases.png)

![database table](/docs/data/airtable_database_table.png)

Make sure you don't left any line without values, because they will be returned as null when you fetch data

[Airtable Documentation](<https://airtable.com/app3CHbSH0ngO1AT7/api/docs>)

- Fetching data

Install package `airtable-node`:

```bash
npm i airtable-node --save
```

Check usage : <https://www.npmjs.com/package/airtable-node#usage>