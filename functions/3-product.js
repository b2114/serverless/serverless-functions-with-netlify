require('dotenv').config()

const Airtable = require('airtable-node');
const tableName = "products";

const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY })
  .base(process.env.AIRTABLE_BASE_ID) // ID that refers the base in a workspace
  .table(tableName)

exports.handler = async (event,context, cb) => {
    const { id } = event.queryStringParameters
    console.log(id)
    if(id){
        try{
            const product = await airtable.retrieve(id);
            if(product.error){
                return {
                    statusCode: 404,
                    body: `Product with id:${id} - Not Found`,
                }
            }
            return {
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                statusCode: 200,
                body: JSON.stringify(product),
            }
        }catch(error){
            console.log(error)
            return {
                statusCode: 500,
                body: `Internal Server Error`,
            }
        }
    }
    return {
        statusCode: 400,
        body: "Please provide product id",
    }
}